<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="content center">
        <div class="container">
            <div class="main">
                <div class="my-row">
                    <button>Họ và tên</button>

                    <input type="text" size="30">
                </div>
                <div class="my-row">
                    <button>Giới tính</button>
                    
                    <?php 
                        $gender = array("Nam", "Nữ");
                        for ($i = 0; $i < count($gender); $i++) {
                            echo "  <div class='margin-5'>
                                        <input type='radio' name='gender' id='gender-$gender[$i]' value=$i>
                                        <label for='gender-$gender[$i]' class='margin-l5'>$gender[$i]</label>
                                    </div>
                            ";
                        }
                    ?>
                </div>
                <div class="my-row">
                    <button>Phân khoa</button>

                    <div class="flex relative">
                        <input list="browsers" name="browser" id="browser" size="11">
                        <datalist id="browsers">
                            <?php
                                $departments = [
                                    "MAT" => "Khoa học máy tính", 
                                    "KDL" => "Khoa học vật liệu"
                                ];
                                foreach ($departments as $key => $val) {
                                    echo "<option value=$key>$val</option>";                        
                                }
                            ?>  
                        </datalist>
                        <div class="triangle"></div>
                    </div>
                </div>
                <div class="submit-container center">
                    <button>Đăng ký</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>